import argparse


def distance(x1, y1, x2, y2):
    return abs(((x2 - x1) ** 2 + (y2 - y1) ** 2) ** (1 / 2))


def center(x1, y1, x2, y2):
    return [(x1 + x2) / 2, (y1 + y2) / 2]


parser = argparse.ArgumentParser(description='Find the root of the equation on an arbitrary interval with a given '
                                             'accuracy.')

parser.add_argument('x1', type=int)
parser.add_argument('y1', type=int)
parser.add_argument('x2', type=int)
parser.add_argument('y2', type=int)
parser.add_argument('--accuracy', type=float, required=True)
args = parser.parse_args()

x1 = args.x1
y1 = args.y1
x2 = args.x2
y2 = args.y2
accuracy = args.accuracy

if (y1 > 0 & y2 > 0) | (y1 < 0 & y2 < 0):
    print("There is no root of the equation.")

if y1 == 0:
    print(f"The root of the equation is x = {x1}, y = {y1}")

if y2 == 0:
    print(f"The root of the equation is x = {x2}, y = {y2}")

center_point = center(x1, y1, x2, y2)
x_line_point = [center_point[0], 0]

while distance(center_point[0], center_point[1], x_line_point[0], x_line_point[1]) > accuracy:
    if distance(x_line_point[0], x_line_point[1], x1, y1) < distance(x_line_point[0], x_line_point[1], x2, y2):
        x2 = center_point[0]
        y2 = center_point[1]
    else:
        x1 = center_point[0]
        y1 = center_point[1]

    center_point = center(x1, y1, x2, y2)
    x_line_point = [center_point[0], 0]

print(f"The root of the equation is x = {center_point[0]}, y = {center_point[1]}")
