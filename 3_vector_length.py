import argparse

parser = argparse.ArgumentParser(description='Function that implements the scalar product of two vectors of arbitrary '
                                             'length.')
parser.add_argument('--vector', type=int, nargs='+', required=True)
args = parser.parse_args()

vector = args.vector
vector_sum = 0

for vector_value in vector:
    vector_sum += vector_value ** 2

vector_length = vector_sum ** 0.5

print(f"Vector length: {vector_length}")
