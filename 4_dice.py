import argparse


def product(*args, repeat=1):
    pools = [tuple(pool) for pool in args] * repeat
    result = [[]]
    for pool in pools:
        result = [x + [y] for x in result for y in pool]
    for prod in result:
        yield tuple(prod)


parser = argparse.ArgumentParser(description='Dice thrown N times. What is the probability that M will drop a number '
                                             'not less than S. Write an algorithm for solving this problem, '
                                             'for any N, M and S.')
parser.add_argument('N', type=int, help='Number of dice rolls')
parser.add_argument('M', type=int, help='Number of cubes with the correct numbers')
parser.add_argument('S', type=int)
args = parser.parse_args()

n = args.N
m = args.M
s = args.S

dice_outcomes = 6
outcomes = 6 * n

dice_variants = [1, 2, 3, 4, 5, 6]
all_variants = list(product(dice_variants, repeat=n))
favorable_outcomes = list(filter(lambda variant: len(list(filter(lambda number: number >= s, variant))) == m, all_variants))

print(f"Probability: {round(len(favorable_outcomes) / len(all_variants) * 100, 2)}%")
