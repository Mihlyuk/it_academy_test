test_graph = {'A': [('B', 10), ('D', 5)],
              'B': [('A', 10), ('C', 10)],
              'C': [('B', 10), ('E', 5), ('F', 12)],
              'D': [('A', 5), ('E', 5)],
              'E': [('D', 5), ('C', 5)],
              'F': [('C', 12)]}


def path_length(path):
    return sum(map(lambda x: x[1], path))


def find_shortest_path(graph, start, end, path=[]):
    if type(start) == str:
        start = (start, 0)

    path = path + [start]
    shortest = None

    if start[0] == end:
        return path

    if start[0] not in graph:
        return None

    for node in graph[start[0]]:
        if node[0] not in list(map(lambda x: x[0], path)):
            new_path = find_shortest_path(graph, node, end, path)

            if new_path:
                if not shortest or path_length(new_path) < path_length(shortest):
                    shortest = new_path

    return shortest


point_1 = 'A'
point_2 = 'F'

print(f"Shortest path from point {point_1} to point {point_2}: "
      f"{list((map(lambda x: x[0], find_shortest_path(test_graph, 'A', 'F'))))}")
