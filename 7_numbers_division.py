import argparse

parser = argparse.ArgumentParser(description='The number of N-digit numbers divisible by M.')

parser.add_argument('N', type=int)
parser.add_argument('M', type=int)
args = parser.parse_args()

n = args.N
m = args.M

first_number = int('1' + '0' * (n - 1))
second_number = int('9' * n)

result = len(list(filter(lambda x: x % m == 0, range(first_number, second_number))))

print(f"The number of {n}-digit numbers divisible by {m} = {result}")
